def average(array):
    # your code goes here
    set_a = set(array)
    return sum(set_a)/len(set_a)

if __name__ == '__main__':
    n = int(input())
    arr = list(map(int, input().split()))
    result = average(arr)
    print(result)
